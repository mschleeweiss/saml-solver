import {startSAMLSolverServer} from "./index";
import process from 'process';
import path from 'path';


function printHelp() {

}

async function loadConfig(path) {
    try {
        return await import(path);
    } catch (e) {
        console.error(e);
        throw e;
    }
}

let configFile = undefined;
let port = undefined;
let expected = undefined;
for(let i = 2; i < process.argv.length; i++) {

    if(expected === 'port')
        port = parseInt(process.argv[i]);

    if(expected) {
        expected = undefined;
        continue;
    }

    if(process.argv[i] === '--help')
        printHelp();
    else if(process.argv[i] === '--port' || process.argv[i] === '-p')
        expected = 'port';
    else
        configFile = path.resolve(process.argv[i]);
}

console.log("Config file: ", configFile);

loadConfig(configFile).then(configModule => {
    const config = configModule.default;

    if (port)
        config.port = port;

    let optionals = {};
    ["urlBlacklist", "pathMappings", "manualInteractions", "keepaliveFreq", "authConfig"]
        .forEach(key => {
            if ( key in config )
                optionals[key] = config[key];
        });

    startSAMLSolverServer(config.port, config.user, config.password, config.baseUrl, config.initPath, optionals);
});

