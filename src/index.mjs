import getSamlSolver from "./solver/index";
import startServer from "./standalone/index";

export const getSAMLSolverProxy = getSamlSolver;
export const startSAMLSolverServer = startServer;
