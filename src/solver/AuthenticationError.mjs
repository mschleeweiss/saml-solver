export class AuthenticationError extends Error {
    constructor(message, status, response) {
        super();
        this.message = message;
        this.httpStatus = status;
        this.response = response;
    }

    toString() {
        return this.message;
    }
}
