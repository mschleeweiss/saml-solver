export const LOG_LEVELS = ['DEBUG', 'INFO', 'WARN', 'ERROR'];

export class Logger {

    constructor(name, logLevel = 'WARN') {
        this.name = name;
        this.logLevel = logLevel;

        this.logWithLevel = {
            'DEBUG': console.debug,
            'INFO': console.log,
            'WARN': console.warn,
            'ERROR': console.error
        };

        this.info = this.log.bind(this, 'INFO');
        this.debug = this.log.bind(this, 'DEBUG');
        this.warn = this.log.bind(this, 'WARN');
        this.error = this.log.bind(this, 'ERROR');
    }

    log(level, message) {
        if( LOG_LEVELS.indexOf(level) === -1 )
            level = 'INFO';
        if(LOG_LEVELS.indexOf(level) >= LOG_LEVELS.indexOf(this.logLevel)) {
            this.logWithLevel[level](`${this.name}|${(new Date()).toUTCString()}|${level}| ${message}`);
        }
    }

}

