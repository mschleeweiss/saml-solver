import tough from "tough-cookie";
import {Semaphore} from "./Semaphore";
import {AuthenticationError} from "./AuthenticationError";
import {Logger} from './Logger';
import puppeteer from "puppeteer";
import urlLib from "url";

let log;

export class SAMLAuthenticator {
    constructor(user, password, baseUrl, debug, authConfig, manualInteractions, urlBlackList ) {
        log = new Logger(this.constructor.name, debug ? 'DEBUG' : 'WARN');

        this.user = user;
        this.password = password;
        this.baseURL = baseUrl;
        this.debug = debug;
        this.authConfig = authConfig
        this.manualInteractions = manualInteractions;
        this.urlBlackList = urlBlackList;

        this.authCookieStore = new tough.MemoryCookieStore();
        this.browser = undefined;
        this.authenticationLock = Promise.reject('This is not an error.').catch(() => {});
        this.authenticationLock.isRejected = true;
        this.authenticationLock.isFullfilled = false;
        this.authenticationLock.isPending = false;
    }

    isAuthenticated() {
        return this.authenticationLock.isFullfilled;
    }

    sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    _getAuthenticationLock(promise) {

        promise.isFullfilled = false;
        promise.isRejected = false;
        promise.isPending = true;

        promise.then(() => {
            promise.isPending = false;
            promise.isFullfilled = true;
        }).catch((err) => {
            promise.isPending = false;
            promise.isRejected = true;
            throw err;
        });
        return promise;
    }

    async ensureAuthenticated(url, path) {

        if( this.authenticationLock.isRejected ) {
            this.authenticationLock = this._getAuthenticationLock(this._authenticate(url, path));
        }

        try {
            await this.authenticationLock;
        } catch(err) {
            throw err;
        }
    }

    patchRequest(req) {

        let cookieStr = this._getCookieJar().getCookieStringSync(this.baseURL);
        if(req.headers['Cookie'])
            req.headers['Cookie'] += ";" + cookieStr;
        else
            req.headers['Cookie'] = cookieStr;
    }

    async _authenticate(url, path) {

        if( this.isAuthenticated() )
            return;

        if(!this.authenticationLock)
            log.warn("Usage of SAMLSolver._authenticate without assigning the authentication lock!");

        const uri = url + path;

        log.debug(`Going to authenticate against ${url + path}`);

        let page;
        try {
            if(!this.browser)
                this.browser = await puppeteer.launch({headless: !this.debug});
            const context = await this.browser.createIncognitoBrowserContext();
            page = await context.newPage();
        } catch (err) {
            log.error("Error while setting up puppeteer!");
            log.error(err);
            throw err;
        }

        const cookieStorePromises = [];

        let requestCounter = new Semaphore();
        const requests = [];

        page.on("request", request => {
            if( !this._isAssetOrBlacklisted(request)) {
                log.debug(`Requesting '${request.url()}`);
                requestCounter.increase();
                requests.push(request.url());
            }
        });

        page.on("response", response => {

            if(!this._isAssetOrBlacklisted(response.request()))
                log.debug(`Status [${response.url()}]: ${response.status()}`);

            if (response.status() >= 400 && response.status() <= 600 && !this._isAssetOrBlacklisted(response.request())) {
                requestCounter.decrease();
                cookieStorePromises.push(Promise.reject(
                    new AuthenticationError(
                        `Error: Received ${response.status()} while requesting ${response.request().url()}!`,
                        response.status()))
                );
            } else {
                cookieStorePromises.push(
                    this._storeCookies(response.headers()['set-cookie'])
                        .then(() => {
                            if( !this._isAssetOrBlacklisted(response.request())) {
                                requestCounter.decrease();
                                requests.splice(requests.indexOf(response.request().url()), 1);
                            }
                        })
                );
            }
        });

        try {
            await page.authenticate({username: this.user, password: this.password});
            log.debug("Waiting until no more requests are being made...");

            await page.goto(uri);
            await page.waitForNavigation({ waitUntil: 'networkidle0' });
        } catch (err) {
            log.error("An error occured while performing SAML authentication flow!");
            throw err;
        }

        // in some cases, we need to manually click a button during the saml protocol
        // An element to click on is given as an object in the form {'url': 'css selector'}
        // Other actions than button presses are currently not supported
        log.info(Object.keys(this.manualInteractions));

        const pageInteraction = this.manualInteractions[page.url()];
        if (pageInteraction) {
            requestCounter = new _Semaphore.Semaphore();
            log.debug(`manually pressing button ( click(${ pageInteraction }) )...`);

            try {
                await page.evaluate((pageInteraction) => document.querySelector(pageInteraction).click(), pageInteraction);
                await page.waitForNavigation({ waitUntil: 'networkidle0' });
            } catch (err) {
                log.error("An error occured while clicking button (from 'manualInteractions')!");
                throw err;
            }

                log.debug(`Waiting for all requests to complete again (${requestCounter.count} pending)...`);
                log.debug(requests);
            try {
                await requestCounter.wait(300000);
            } catch (err) {
                log.error("Timeout received while handling manual interactions (button click)!")
            }
        }

        try {
            if (this.authConfig.user !== undefined) {

                await page.$eval(this.authConfig.user, (el, username) => el.value = username, this.user);
                await page.$eval(this.authConfig.password, (el, pw) => el.value = pw, this.password);
                await page.click(this.authConfig.submit);

                log.debug("Waiting until no more requests are being made...");

                await page.goto(uri, {waitUntil: "networkidle0"});
            }
        } catch (err) {
            log.error("An error occured while performing SAML authentication flow!");
            throw err;
        }

        log.debug(`Waiting for all requests to complete (${requestCounter.count} pending)...`);
        log.debug(requests);

        try {
            await requestCounter.wait(300000);
        } catch (err) {
            log.error("Timeout while waiting for SAML authentication requests to complete!");
            throw err;
        }

        log.debug("Waiting until all cookies have been collected...");

        try {
            await Promise.all(cookieStorePromises);
        } catch (err) {
            log.error("Error while retrieving SAML token!");
            throw err;
        }

        log.debug("Authentication complete.");
        log.debug("Collected cookies:");
        log.debug(this._getCookieJar().getCookieStringSync(this.baseURL));

        let authenticated = false;
        this._getCookieJar().getCookieStringSync(this.baseURL).split(";").forEach(cookie => {
            let cookie_split = cookie.split("=");
            if ( cookie_split[0].trim() === "JSESSIONID" ) {
                authenticated = true;
            }
        });
        setTimeout(async () => {
            try {
                await this.browser.close();
            } catch (err) {
                log.warn(`An error occured while closing the puppeteer browser!\n${err}`);
            }
        }, 30000);

        if(!authenticated)
            throw new AuthenticationError("Authentication failed for unknown reasons (JSESSIONID cookie has not been set)!");

    }

    async _storeCookies(cookieString) {
        if( !cookieString )
            return;

        const cookieList = cookieString.split("\n").map(tough.Cookie.parse);

        await Promise.all(cookieList.map(SAMLAuthenticator._storeCookie.bind(this, this.authCookieStore)));

    }

    static async _storeCookie(cookieStore, cookie) {
        // if the requested cookie exists already, update it and create it otherwise
        await new Promise((resolve, reject) => {
            cookieStore.findCookie(cookie.domain, cookie.path, cookie.key, (err, c) => {
                if (c === null) {
                    cookieStore.putCookie(cookie, (err) => {
                        if (err)
                            reject(err);
                        resolve();
                    });
                } else {
                    cookieStore.updateCookie(c, cookie, (err) => {
                        if (err)
                            reject(err);
                        resolve();
                    });
                }
            });
        });
    }

    _getCookieJar() {
        const jar = new tough.CookieJar(this.authCookieStore);

        // TODO: This is a rather nasty hack. Can we do better?
        this.authCookieStore.getAllCookies((err, cookies) => {
            cookies.forEach(cookie => {
                let domain = /(?<=Domain=).*?(?=;)/.exec(cookie)
                jar.setCookieSync(cookie, domain ? `https://${domain}` : this.baseURL);
            });
        });
        return jar;
    }

    _isAssetOrBlacklisted(req) {
        const url = urlLib.parse(req.url());

        const lastDotPos = url.pathname.lastIndexOf(".");

        const extension = url.pathname.substr(lastDotPos);

        const assetExtensions = ['.ico', '.css', '.png', '.jpg', '.jpeg', '.svg', '.css'];
        const assetResourceTypes = ['stylesheet', 'image', 'media', 'font'];
        const isAsset = assetExtensions.indexOf(extension) !== -1 || assetResourceTypes.indexOf(req.resourceType()) !== -1;

        if (isAsset)
            log.debug("Asset detected!");

        const isBlacklisted = this.urlBlackList.reduce(
            (matched, blacklistedURL) =>  matched || (url.href.indexOf(blacklistedURL) !== -1), false);

        if (isBlacklisted)
            log.debug(`URL '${url.href}' is blacklisted! (errors/timeouts will be ignored)`);

        return isAsset || isBlacklisted;
    }

}
