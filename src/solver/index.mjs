import {SAMLSolver} from "./SAMLSolver";


export default function(user, pw, baseURL, samlInitPath, optionals) {
    let solver = new SAMLSolver(user, pw, baseURL, samlInitPath, optionals);
    return solver.resolve.bind(solver);
};
